<x-filament-panels::page>
    <div class="relative flex">
        <div class="flex-auto max-w-none min-w-0">

        <div class="pb-10" id="syntheseSection">
        {{ $this->requestInfolist }}
        </div>

        <x-filament-panels::form wire:submit="updateStep" id="stepSection">
            {{ $this->stepForm }}
            

            <div class="fi-ac gap-3 flex flex-wrap items-center justify-end pb-10">
                <x-filament::button wire:click="updateStep">Enregistrer</x-filament::button>
            </div>

        </x-filament-panels::form>
        <x-filament-panels::form wire:submit="updateRequest" id="requestSection">
            {{ $this->requestForm }}
            
            {{-- <x-filament-panels::form.actions 
                :actions="$this->getUpdateRequestFormActions()"
            /> --}}

            <div class="fi-ac gap-3 flex flex-wrap items-center justify-end pb-10">
                <x-filament::button wire:click="updateRequest">Enregistrer</x-filament::button>
            </div>

        </x-filament-panels::form>
        <x-filament-panels::form wire:submit="updateAdmin" id="adminSection">
            {{ $this->adminForm }}
            
            {{-- <x-filament-panels::form.actions 
                :actions="$this->getUpdateAdminFormActions()"
            /> --}}
            <div class="fi-ac gap-3 flex flex-wrap items-center justify-end pb-10">
                <x-filament::button wire:click="updateAdmin">Enregistrer</x-filament::button>
            </div>
        </x-filament-panels::form>
        </div>
        <div class="sticky top-14 -mr-6 block h-screen flex-none overflow-y-auto pt-5 pb-16 pr-0 navigation-custom-scrollbar">
            <div class="w-56 pl-8 mr-10">
                    <h2 class="fi-header-heading text-base font-bold tracking-tight text-gray-950 dark:text-white sm:text-base  mb-2">Dans cette page </h2>
                    <a href="#syntheseSection" class="inline-block group-hover/link:underline group-focus-visible/link:underline text-sm text-custom-600 dark:text-custom-400 mb-3" style="--c-400:var(--primary-400);--c-600:var(--primary-600);">Synthèse</a>
                    <a href="#stepSection" class="inline-block group-hover/link:underline group-focus-visible/link:underline text-sm text-custom-600 dark:text-custom-400 mb-3" style="--c-400:var(--primary-400);--c-600:var(--primary-600);">Processus de soumission</a>
                    <a href="#requestSection" class="inline-block group-hover/link:underline group-focus-visible/link:underline text-sm text-custom-600 dark:text-custom-400 mb-3" style="--c-400:var(--primary-400);--c-600:var(--primary-600);">Fomulaire de réponse</a>
                    <a href="#adminSection" class="inline-blockgroup-hover/link:underline group-focus-visible/link:underline text-sm text-custom-600 dark:text-custom-400 mb-3" style="--c-400:var(--primary-400);--c-600:var(--primary-600);">Administration de la demande de renseignements</a>
            </div>
        </div>
    </div>
</x-filament-panels::page>
