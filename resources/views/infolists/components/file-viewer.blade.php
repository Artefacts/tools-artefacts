<x-dynamic-component :component="$getEntryWrapperView()" :entry="$entry">
    <div class="flex flex-wrap">
        <div class="w-1/3 p-1">
            <embed type="{{ $entry->getMime( $getState() ) }}" src="{{ url('/') }}/private/view/{{ $getState() }}" width="100" height="150" style="width: 100px;object-fit: contain;object-position: 50% top;" />
        </div>
        <div class="flex-none w-1/3 p-1">
            <x-filament::modal width="5xl" sticky-header>
                <x-slot name="trigger">
                    <x-filament::button size="sm" color="gray" icon="heroicon-o-document-magnifying-glass">
                        Preview >
                    </x-filament::button>
                </x-slot>
                <x-slot name="heading">
                    Preview of {{ $getState() }}
                </x-slot>
                <embed type="{{ $entry->getMime( $getState() ) }}" src="{{ url('/') }}/private/view/{{ $getState() }}" width="100%" height="800" style="width: 100%; object-fit: contain;object-position: 50% top;" />
            </x-filament::modal>
        </div>
        <div class="flex-none w-1/3 p-1">
            
            <x-filament::button size="sm" color="gray" icon="heroicon-o-arrow-down-tray"
            href="{{ url('/') }}/private/download/{{ $getState() }}"
            tag="a" >
                        Dowload
            </x-filament::button>
        </div>
    </div>
</x-dynamic-component>
