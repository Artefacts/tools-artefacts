<div>
    <form wire:submit="save">
        {{ $this->form }}

        <x-filament::button
                    type="submit"
                    size="sm"
                >
                    Submit
                </x-filament::button>
    </form>

    <x-filament-actions::modals />
</div>
