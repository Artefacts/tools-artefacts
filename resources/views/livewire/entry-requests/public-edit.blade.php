<div>
    @if ($this->record->step->editableEntryRequest())
    <form wire:submit="save">
        {{ $this->form }}

        <x-filament::button
                    type="submit"
                    size="sm"
                >
                    Soumettre à l'admin
        </x-filament::button>

    </form>

    <x-filament-actions::modals />

    @else

    Cette demande n'est plus éditable

    @endif
</div>
