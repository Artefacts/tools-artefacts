<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use stdClass;
use App\Observers\EntryRequestObserver;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use App\Notifications\NewEntryRequested;
use Illuminate\Support\Facades\Log;
use Spatie\ModelStates\HasStates;
use App\States\EntryRequest\RequestState;
use App\States\EntryRequest\Open;
use App\States\EntryRequest\Pulled;
use App\Models\EntryRequestForm;
use App\Models\EntryRequestLog;
use Illuminate\Support\Arr;

#[ObservedBy([EntryRequestObserver::class])]

class EntryRequest extends Model
{
    use HasFactory;
    use Notifiable;
    use HasStates;

    protected $observables = ['forgottenFilling'];

    protected $casts = [
        'step' => RequestState::class,
        'answers' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'answers',
        'instructions',
        'admin_email',
    ];

    
    
    /**
     * Get the log associated with the entry request.
     */
    public function isLogDirty($attributeName): bool
    {
        if($this->log === null) {
            return false ;
        }
        return Arr::exists($this->log->diff, $attributeName);
    }


    
    /**
     * Get the log associated with the entry request.
     */
    public function log(): HasOne
    {
        return $this->hasOne(EntryRequestLog::class);
    }

    public function form(): BelongsTo
    {
        return $this->belongsTo(EntryRequestForm::class);
    }


    /**
     * Route notifications for the mail channel.
     *
     * @return  array<string, string>|string
     */
    public function routeNotificationForMail(Notification $notification): array|string
    {
        // Return email address only...
        //Log::info(print_r($notification, true));
        if (is_a($notification, 'App\Notifications\EntryPushed')) {
            return $this->email.'ttttt';
        }
        return $this->email;

    }

    
    public function forgottenFilling($record){
        
    }
    public function RejectedFilling($record){
        
    }

    public function getAssignblesSteps() {
        
        //Log::info(print_r('getAssignblesSteps', true));
        $Steps = $this->step->transitionableStates();
        $returnStep = [] ;
        foreach($Steps as $key => $step) {
                $stateClass = RequestState::resolveStateClass($step);
                $stateClassStatic  = new $stateClass($this);
        //Log::info(print_r($stateClass, true));
                $returnStep[$step] = $stateClassStatic->actionLabel() ;
        }
        //Log::info(print_r($returnStep, true));
        return $returnStep ;

    }

    public function getAssignblesStepsColors() {
        
        //Log::info(print_r('getAssignblesSteps', true));
        $Steps = $this->step->transitionableStates();
        $returnStep = [] ;
        foreach($Steps as $key => $step) {
                $stateClass = RequestState::resolveStateClass($step);
                $stateClassStatic  = new $stateClass($this);
        //Log::info(print_r($stateClass, true));
                $returnStep[$step] = $stateClassStatic->color() ;
        }
        //Log::info(print_r($returnStep, true));
        return $returnStep ;

    }
    public function getAssignblesStepsLabelsColors() {
        
        //Log::info(print_r('getAssignblesSteps', true));
        $Steps = EntryRequest::getStatesFor('step');
        $returnStep = [];
        foreach($Steps as $key => $step) {
                $stateClass = RequestState::resolveStateClass($step);
                $stateClassStatic  = new $stateClass($this);
        //Log::info(print_r($stateClass, true));
        $label = $stateClassStatic->label();
                $returnStep[$label] = $stateClassStatic->color() ;
        }
        //Log::info(print_r($returnStep, true));
        return $returnStep ;

    }
    
}
