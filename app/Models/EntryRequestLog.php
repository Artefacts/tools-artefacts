<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\EntryRequest;
use Illuminate\Support\Facades\Log;

class EntryRequestLog extends Model
{
    use HasFactory;

        
    protected $casts = [
        'ref' => 'array',
        'diff' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'ref',
        'diff',
    ];

    public function reset() {
        $this->ref = null ;
        $this->diff = null ;
        $this->save() ;
        
    }
    public function initLog(EntryRequest $entryRequest) {
        
        //Log::info(print_r('Init log', true));
        //Log::info(print_r($entryRequest, true));
        $this->ref = $entryRequest->attributesToArray() ;
        if(isset($this->ref['answers'])) {
            //$this->ref['answers'] = json_decode($this->ref['answers'], true);
        }
        //Log::info(print_r($this->ref, true));
        $this->diff = array() ;
        $this->save() ;
        //Log::info(print_r($this, true));
        
    }

    
    public function updateDiff(EntryRequest $entryRequest) {
        if ($this->ref == null) {
            $this->initLog($entryRequest);
        }
        $new = $entryRequest->attributesToArray() ;
        $old = $entryRequest->getOriginal() ;

        $entityChamges = $entryRequest->getChanges() ;
        if (isset($entityChamges['answers'])) {
            $entityChamges['answers'] = json_decode($entityChamges['answers'], true);
        }


        /*Log::info(print_r($new, true));
        Log::info(print_r($old, true));*/
        

        $changes = array_merge($this->diff, $entityChamges);
        /*Log::info(print_r('changes', true));
        Log::info(print_r($this->diff, true));
        Log::info(print_r($entityChamges, true));
        Log::info(print_r($changes, true));
        Log::info(print_r($this->ref, true));*/


        /*
        
        Log::info(print_r($this->diff, true));*/
        //$diff = array()  ;
        //$this->diff = $changes ;
        
        if(isset($changes['answers'])) {
            //$changes['answers'] = json_decode($changes['answers'], true);
        }
        $this->diff= $this->arrayRecursiveDiff($changes, $this->ref );
        //$this->diff = array() ;
        //$this->diff = $diff ;
        $this->save() ;
        
    }

    /**
     * Get the EntryRequest that owns the log.
     */
    public function entryRequest(): BelongsTo
    {
        return $this->belongsTo(EntryRequest::class);
    }

    
    public function arrayRecursiveDiff($aArray1, $aArray2) {

        /*Log::info(print_r($aArray1, true));
        Log::info(print_r($aArray2, true));*/
        $aReturn = array();
        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff; }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }

}
