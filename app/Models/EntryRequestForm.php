<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use App\Models\EntryRequest;

class EntryRequestForm extends Model
{
    use HasFactory;

       
    protected $casts = [
        'open' => 'boolean',
        'fields' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'fields',
        'open',
    ];


    public function entryRequest(): HasMany
    {
        return $this->hasMany(EntryRequest::class);
    }

}
