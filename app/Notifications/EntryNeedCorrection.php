<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Mail\Mailable;

class EntryNeedCorrection extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $url = url('/requests/edit/'.$notifiable->id);

        return (new MailMessage)
                    ->subject('Dossier d\'entré à Artefact : Des corrections sont nécessaires')
                    ->line('Ton dossier n\'est pas correcte.')
                    ->lineIf($notifiable->instructions != null, "Instructions : {$notifiable->instructions}")
                    ->action('Corriger mon dossier', $url)
                    ->line('Meri de le remplir assez vite.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
