<?php

namespace App\Observers;

use App\Models\EntryRequest;
use App\Models\EntryRequestLog;
use Illuminate\Notifications\Notifiable;
use App\Notifications\NewEntryRequested;
use App\Notifications\EntryNeedCorrection;
use App\Notifications\EntryPushed;
use Illuminate\Support\Facades\Log;
use App\States\EntryRequest\Open;
use App\States\EntryRequest\Pulled;

class EntryRequestObserver
{
    /**
     * Handle the EntryRequest "created" event.
     */
    public function creating(EntryRequest $entryRequest): void
    {
        //
    }
    /**
     * Handle the EntryRequest "created" event.
     */
    public function created(EntryRequest $entryRequest): void
    {
        //
        $entryRequest->notify(new NewEntryRequested($entryRequest));

        
        $log = new EntryRequestLog() ;
        $log->initLog($entryRequest) ;
        $entryRequest->log()->save($log) ;
    }

    /**
     * Handle the EntryRequest "created" event.
     */
    public function updating(EntryRequest $entryRequest): void
    {
        
    }

    /**
     * Handle the EntryRequest "updated" event.
     */
    public function updated(EntryRequest $entryRequest): void
    {
        $anonymousFields =  [
            'info_1',
            'info_2',
            'fichier_1',
            'fichier_2',
        ] ;
        
        $entryRequest->log->updateDiff($entryRequest) ;

        if($entryRequest->wasChanged('step') ) {
            
            //$entryRequest->step->transitionTo($entryRequest->step, 'normal process');

            /*if ($entryRequest->wasChanged($anonymousFields)) {
                if($entryRequest->step != 'pulled') {
                    //
                }
            }*/
        }

        
        //Log::info(print_r($entryRequest, true));
    }

    /**
     * Handle the EntryRequest "deleted" event.
     */
    public function deleted(EntryRequest $entryRequest): void
    {
        //
    }

    /**
     * Handle the EntryRequest "restored" event.
     */
    public function restored(EntryRequest $entryRequest): void
    {
        //
    }

    /**
     * Handle the EntryRequest "force deleted" event.
     */
    public function forceDeleted(EntryRequest $entryRequest): void
    {
        //
    }


    
    /**
     * Handle the EntryRequest "forgottenFilling" event. Request waiting longuest response
    */
    public function forgottenFilling(EntryRequest $entryRequest){
        $entryRequest->notify(new NewEntryRequested($entryRequest));
    }
    
    /**
     * Handle the EntryRequest "RejectedFilling" event. Response is not good
    */
    public function RejectedFilling(EntryRequest $entryRequest){
        $entryRequest->notify(new EntryNeedCorrection($entryRequest));
    }
}
