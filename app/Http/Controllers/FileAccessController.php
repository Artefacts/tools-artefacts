<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class FileAccessController extends Controller
{
    // In FileAccessController.php
    public function serve($file)
    {

        if(Auth::user()) {
            // Here we don't use the Storage facade that assumes the storage/app folder
            // So filename should be a relative path inside storage to your file like 'app/userfiles/report1253.pdf'
            $path = Storage::path('private/form-attachments/'.$file);
            return response()->file($path);
        }else{
            return abort('404');
        }
    }
    // In FileAccessController.php
    public function download($file)
    {
        // We should do our authentication/authorization checks here
        // We assume you have a FileModel with a defined belongs to User relationship.
        if(Auth::user()) {
            // filename should be a relative path inside storage/app to your file like 'userfiles/report1253.pdf'
            return Storage::download('private/form-attachments/'.$file);
        }else{
            return abort('403');
        }
    }
}
