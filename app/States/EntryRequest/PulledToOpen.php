<?php

namespace App\States\EntryRequest;

use Spatie\ModelStates\Transition;
use App\Models\EntryRequest;
use App\Notifications\EntryPushed;
use App\States\EntryRequest\Open;
use Illuminate\Support\Facades\Log;

class PulledToOpen extends Transition
{
    private EntryRequest $EntryRequest;

    private $message;

    public function __construct(EntryRequest $entryRequest, $message='')
    {
        $this->EntryRequest = $entryRequest;

        $this->message = $message;
    }

    public function handle(): EntryRequest
    {
        
        Log::info(print_r('Pulled to Open', true));
        $this->EntryRequest->notify(new EntryPushed($this->EntryRequest));
        $this->EntryRequest->step = new Open($this->EntryRequest);
        $this->EntryRequest->save();
        $this->EntryRequest->log->reset() ;
        $this->EntryRequest->log->initLog($this->EntryRequest) ;

        return $this->EntryRequest;
    }
}