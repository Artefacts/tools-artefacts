<?php

namespace App\States\EntryRequest;

use Spatie\ModelStates\Transition;
use App\Models\EntryRequest;
use App\Notifications\EntryPushed;
use App\States\EntryRequest\Pulled;
use Illuminate\Support\Facades\Log;

class OpenToPulled extends Transition
{
    private EntryRequest $EntryRequest;

    private  $message;

    public function __construct(EntryRequest $entryRequest, $message='')
    {
        $this->EntryRequest = $entryRequest;

        $this->message = $message;
    }

    public function handle(): EntryRequest
    {
        
        $this->EntryRequest->notify(new EntryPushed($this->EntryRequest));

        $this->EntryRequest->step = new Pulled($this->EntryRequest);
        $this->EntryRequest->save() ;
        
        //$this->EntryRequest->log->initLog($this->EntryRequest) ;

        return $this->EntryRequest;
    }
}