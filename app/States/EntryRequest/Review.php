<?php

namespace App\States\EntryRequest;

use App\States\EntryRequest\RequestState;

class Review extends RequestState
{
    public static $name = 'review';

    public function color(): string
    {
        return 'warning';
    }
    
    public function label(): string
    {
        return 'En révision';
    }
    
    public function actionLabel(): string
    {
        return 'Faire réviser';
    }

    public function editableEntryRequest(): bool
    {
        return true;
    }
}