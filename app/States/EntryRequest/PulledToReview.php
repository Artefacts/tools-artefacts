<?php

namespace App\States\EntryRequest;

use Spatie\ModelStates\Transition;
use App\Models\EntryRequest;
use App\Notifications\EntryNeedCorrection;
use App\States\EntryRequest\Review;
use Illuminate\Support\Facades\Log;

class PulledToReview extends Transition
{
    private EntryRequest $EntryRequest;

    private $message;

    public function __construct(EntryRequest $entryRequest, $message='')
    {
        $this->EntryRequest = $entryRequest;

        $this->message = $message;
    }

    public function handle(): EntryRequest
    {
        $this->EntryRequest->instructions = $this->message ;
        $this->EntryRequest->notify(new EntryNeedCorrection($this->EntryRequest));
        $this->EntryRequest->step = new Review($this->EntryRequest);
        $this->EntryRequest->save();
        
        $this->EntryRequest->log->reset() ;
        $this->EntryRequest->log->initLog($this->EntryRequest) ;

        return $this->EntryRequest;
    }
}