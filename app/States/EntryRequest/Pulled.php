<?php

namespace App\States\EntryRequest;

use App\States\EntryRequest\RequestState;

class Pulled extends RequestState
{
    public static $name = 'pulled';

    public function color(): string
    {
        return 'warning';
    }
    
    public function label(): string
    {
        return 'Soumis';
    }
    
    public function actionLabel(): string
    {
        return 'Interrompre la soumission';
    }

    public function editableEntryRequest(): bool
    {
        return false;
    }
}