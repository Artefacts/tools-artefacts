<?php 

namespace App\States\EntryRequest;

use App\States\EntryRequest\RequestState;

class Accepted extends RequestState
{
    public static $name = 'accepted';

    public function color(): string
    {
        return 'success';
    }
    
    public function label(): string
    {
        return 'Accepté';
    }
    
    public function actionLabel(): string
    {
        return 'Accepter';
    }

    public function editableEntryRequest(): bool
    {
        return false;
    }
}