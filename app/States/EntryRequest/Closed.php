<?php

namespace App\States\EntryRequest;

use App\States\EntryRequest\RequestState;

class Closed extends RequestState
{
    public static $name = 'closed';

    public function color(): string
    {
        return 'danger';
    }
    
    public function label(): string
    {
        return 'Clôturé';
    }
    
    public function actionLabel(): string
    {
        return 'Clôturer';
    }

    public function editableEntryRequest(): bool
    {
        return true;
    }
}