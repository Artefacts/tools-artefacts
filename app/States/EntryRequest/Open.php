<?php

namespace App\States\EntryRequest;

use App\States\EntryRequest\RequestState;

class Open extends RequestState
{
    public static $name = 'open';

    public function color(): string
    {
        return 'success';
    }

    public function label(): string
    {
        return 'Ouvert';
    }
    
    public function actionLabel(): string
    {
        return 'Ouvrir';
    }

    public function editableEntryRequest(): bool
    {
        return true;
    }
}