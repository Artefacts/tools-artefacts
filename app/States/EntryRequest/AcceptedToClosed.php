<?php

namespace App\States\EntryRequest;

use Spatie\ModelStates\Transition;
use App\Models\EntryRequest;
use App\Notifications\EntryPushed;
use App\States\EntryRequest\Closed;
use Illuminate\Support\Facades\Log;

class AcceptedToClosed extends Transition
{
    private EntryRequest $EntryRequest;

    private $message;

    public function __construct(EntryRequest $entryRequest, $message='')
    {
        $this->EntryRequest = $entryRequest;

        $this->message = $message;
    }

    public function handle(): EntryRequest
    {
        
        Log::info(print_r('Accepted to Closed', true));
        $this->EntryRequest->step = new Closed($this->EntryRequest);
        $this->EntryRequest->save();
        $this->EntryRequest->log->reset() ;
        $this->EntryRequest->log->initLog($this->EntryRequest) ;

        return $this->EntryRequest;
    }
}