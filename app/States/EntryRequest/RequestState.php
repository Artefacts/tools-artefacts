<?php

namespace App\States\EntryRequest;

use Spatie\ModelStates\State;
use Spatie\ModelStates\StateConfig;
use App\States\EntryRequest\Open;
use App\States\EntryRequest\Pulled;
use App\States\EntryRequest\Review;
use App\States\EntryRequest\Accepted;
use App\States\EntryRequest\Closed;
use App\States\EntryRequest\OpenToPulled;
use App\States\EntryRequest\ReviewToPulled;
use App\States\EntryRequest\ReviewToAccepted;
use App\States\EntryRequest\PulledToOpen;
use App\States\EntryRequest\PulledToReview;
use App\States\EntryRequest\PulledToAccepted;

/**
 * @extends State<\App\Models\EntryRequest>
 */
abstract class RequestState extends State
{
    abstract public function color(): string;

    public static function config(): StateConfig
    {
        return parent::config()
            ->default(Open::class)
            ->allowTransition(Open::class, Pulled::class, OpenToPulled::class)
            ->allowTransition(Pulled::class, Review::class, PulledToReview::class)
            ->allowTransition(Pulled::class, Accepted::class, PulledToAccepted::class)
            ->allowTransition(Review::class, Pulled::class, ReviewToPulled::class)
            ->allowTransition(Review::class, Accepted::class, ReviewToAccepted::class)
            ->allowTransition(Accepted::class, Closed::class, AcceptedToClosed::class)
        ;
    }
}