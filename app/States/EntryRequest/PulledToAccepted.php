<?php

namespace App\States\EntryRequest;

use Spatie\ModelStates\Transition;
use App\Models\EntryRequest;
use App\Notifications\EntryAccepted;
use App\States\EntryRequest\Accepted;
use Illuminate\Support\Facades\Log;

class PulledToAccepted extends Transition
{
    private EntryRequest $EntryRequest;

    private $message;

    public function __construct(EntryRequest $entryRequest, $message='')
    {
        $this->EntryRequest = $entryRequest;

        $this->message = $message;
    }

    public function handle(): EntryRequest
    {
        
        Log::info(print_r('Pulled to Accepted', true));
        $this->EntryRequest->instructions = $this->message ;
        $this->EntryRequest->notify(new EntryAccepted($this->EntryRequest));
        $this->EntryRequest->step = new Accepted($this->EntryRequest);
        $this->EntryRequest->save();
        $this->EntryRequest->log->reset() ;
        $this->EntryRequest->log->initLog($this->EntryRequest) ;

        return $this->EntryRequest;
    }
}