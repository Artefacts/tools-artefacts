<?php

namespace App\Filament\Resources\EntryRequestFormResource\Pages;

use App\Filament\Resources\EntryRequestFormResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListEntryRequestForms extends ListRecords
{
    protected static string $resource = EntryRequestFormResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
