<?php

namespace App\Filament\Resources\EntryRequestFormResource\Pages;

use App\Filament\Resources\EntryRequestFormResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditEntryRequestForm extends EditRecord
{
    protected static string $resource = EntryRequestFormResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
