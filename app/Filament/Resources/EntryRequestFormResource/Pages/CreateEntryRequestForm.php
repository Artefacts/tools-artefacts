<?php

namespace App\Filament\Resources\EntryRequestFormResource\Pages;

use App\Filament\Resources\EntryRequestFormResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateEntryRequestForm extends CreateRecord
{
    protected static string $resource = EntryRequestFormResource::class;
}
