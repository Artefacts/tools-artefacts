<?php

namespace App\Filament\Resources\EntryRequestResource\Pages;

use App\Filament\Resources\EntryRequestResource;
use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;
use Filament\Infolists\Infolist;

class ViewEntryRequest extends ViewRecord
{
    protected static string $resource = EntryRequestResource::class;

    public function infolist(Infolist $infolist): Infolist
    {
        $array_fileds = [] ; 
        $values_fields = $this->record->answers ;
        foreach ($this->record->form->fields as $field) {
            $data = $field['data'] ;
            $answer = $this->record->answers ;
            
            if ($field['type'] == 'form_field') {
                $className = 'Filament\Infolists\Components\TextEntry' ;
                $colspan = 1 ;
                if($data['field_type'] == 'FileUpload') {
                    $className = 'App\Infolists\Components\Fileviewer' ;
                    $colspan = 1 ;
                }
                $name = $data['name'] ;
                if (isset($answer[$name]) ) {
                    $response = $answer[$name] ;
                } else {
                    $response = 'Pas de réponse' ;
                }
                
                if(isset($data['options']) && (sizeof($data['options']) > 0)) {
                    if (isset($data['options'][$response])) {
                        $response = $data['options'][$response] ;
                    }
                }

                $component = $className::make($data['name'])
                    ->label($data['label']) 
                    ->default($response)->columnSpan($colspan);

                
                if($data['field_type'] != 'FileUpload') {

                } else {


                }
                
                array_push($array_fileds, $component) ;
            }
            

        }


        return $infolist
            ->schema($array_fileds);
    }

}
