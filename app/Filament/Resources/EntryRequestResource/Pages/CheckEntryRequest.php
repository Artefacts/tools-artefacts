<?php

namespace App\Filament\Resources\EntryRequestResource\Pages;

use Exception;

use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Infolists\Concerns\InteractsWithInfolists;
use Filament\Infolists\Contracts\HasInfolists;
use Filament\Forms\Form;
use Filament\Infolists\Infolist;
//use Filament\Pages\Page;

use App\Models\EntryRequest;
use App\Models\EntryRequestForm;
use Filament\Forms;
//use Filament\Forms\Form;
//use Filament\Pages\Concerns;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rules\Password;
use Illuminate\Contracts\Auth\Authenticatable;
//use Filament\Forms\Contracts\HasForms;
//use Filament\Forms\Concerns\InteractsWithForms;
use App\Filament\Resources\EntryRequestResource;
use Filament\Resources\Pages\Page;
use Filament\Resources\Pages\Concerns;
use Filament\Resources\Pages\Concerns\InteractsWithRecord;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\ToggleButtons;
use App\States\EntryRequest\Open;
use App\States\EntryRequest\Pulled;
use App\States\EntryRequest\Review;
use Illuminate\Support\Facades\Log;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Split;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Wizard;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\HtmlString;
use Illuminate\Database\Eloquent\Builder;
//use Filament\Resources\Pages\Page;
use App\Filament\Resources\EntryRequestResource\Pages;
use App\Filament\Resources\EntryRequestResource\RelationManagers;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\Grid; 
use Filament\Notifications\Notification;
use Filament\Infolists\Components\Section as iSection;
use Filament\Infolists\Components\TextEntry;

class CheckEntryRequest extends Page implements HasForms
{
    
    use InteractsWithForms;
    use InteractsWithRecord;
    use InteractsWithInfolists;

    protected static string $resource = EntryRequestResource::class;

    protected static string $view = 'filament.resources.entry-request-resource.pages.check-entry-request';

    protected static ?string $title = 'Vérifer une soumission';
    
    protected static ?string $navigationLabel = 'Soumissions';

        
    public ?array $adminData = [];
    public ?array $requestData = [];
    public ?array $stepData = [];


    public function mount(int | string $record): void
    {
        $this->record = $this->resolveRecord($record);
        $this->record = EntryRequest::findOrFail($this->record->id);

        $is_relation = $this->record->isRelation('form') ;
        
        //Log::info(print_r($is_relation, true));

        $this->fillForms();
    }

    public function requestInfolist(Infolist $infolist): Infolist
    {
        return $infolist
            ->record($this->record)
            ->schema([
                iSection::make('Synthèse')
                ->aside() // This is the magic line!
                ->description('Informations globales sur la soumission')
                ->columns(4)
                ->compact()
                ->schema([
                    TextEntry::make('created_at')
                    ->label('Création')
                    ->dateTime()
                    ->since(),
                    TextEntry::make('updated_at')
                    ->dateTime()
                    ->label('Modification')
                    ->since(),
                    TextEntry::make('step')
                    ->label('Etat')
                    ->badge()
                    ->state(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        return $EntryRequest->step->label() ;
                    })
                    ->color(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->getAssignblesStepsLabelsColors(), true));
                        return $EntryRequest->step->color() ;
                    }),
                    TextEntry::make('step.editableEntryRequest')
                    ->label('Formulaire ouvert ?')
                    ->icon(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        if ($EntryRequest->step->editableEntryRequest()) {
                            return 'heroicon-o-lock-open';
                        }
                        return 'heroicon-s-lock-closed' ;
                    })
                    ->iconColor(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        if ($EntryRequest->step->editableEntryRequest()) {
                            return 'success';
                        }
                        return 'danger' ;
                    })
                    ->color(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        if ($EntryRequest->step->editableEntryRequest()) {
                            return 'success';
                        }
                        return 'danger' ;
                    })
                    ->default(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        if ($EntryRequest->step->editableEntryRequest()) {
                            return 'Oui';
                        }
                        return 'Non' ;
                    })
                ])
            ]);
    }

    protected function getForms(): array
    {
        return [
            'stepForm',
            'adminForm',
            'requestForm',
        ];
    }

    public function stepForm(Form $form): Form
    {
        return $form->schema([
        
            Section::make('Processus de soumission')
                ->aside() // This is the magic line!
                ->description('Resoumetre ou valider la demande d\'nformations.')
                ->schema([
                ToggleButtons::make('step')
                    ->label('Changer l\'état du formulaire')
                    ->options(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        return $EntryRequest->getAssignblesSteps() ;
                    }
                    /*[
                        'open' => 'Open',
                        'pulled' => 'Pulled',
                        'review' => 'Review',
                        'published' => 'Published'
                    ]*/
                    )
                    ->colors(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        return $EntryRequest->getAssignblesStepsColors() ;
                    }
                    /*[
                        'open' => 'info',
                        'pulled' => 'primary',
                        'review' => 'warning',
                        'published' => 'success',
                    ]*/
                    )
                    ->inline()
                    ->default(function (EntryRequest $EntryRequest) {
                        //Log::info(print_r($EntryRequest->step->getValue(), true));
                        return $EntryRequest->step->getValue() ;
                    })
                    ->afterStateUpdated(function (Set $set, $state) {
                        /*$class = null ; 
                        if ($state == 'open') {
                            $class = Open::class ;
                        }
                        if ($state == 'pulled') {
                            $class = Pulled::class ;
                        }
                        if ($state == 'review') {
                            $class = Review::class ;
                        }
                        $set('step', $class);*/
                    }),
                Textarea::make('instructions')
                    ->label('Instructions à transmettre par mail')
                    ->rows(10),
            ])->grow(false),
        ])
        ->statePath('stepData')
        ->model($this->record);
    }

    public function adminForm(Form $form): Form
    {
        return $form->schema([
        
            Section::make('Administration de la demande de renseignements')
            ->aside() // This is the magic line!
            ->description('Contact et notifications.')
            ->schema([
                // ...
                    Forms\Components\Select::make('form_id')
                    ->relationship('form', 'name')
                    ->required(),
                    Forms\Components\TextInput::make('email')->regex('/^.+@.+$/i')
                        ->required()
                        ->maxLength(255),
                    Forms\Components\TextInput::make('admin_email')->regex('/^.+@.+$/i')
                        ->maxLength(255),
            ]),
        ])
        ->statePath('adminData')
        ->model($this->record);
    }

    public function requestForm(Form $form): Form
    {
        if($this->record->answers == null) {
            $this->record->answers = [] ;
        }

        $array_fileds = [] ; 
        $values_fields = $this->record->answers ;
        foreach ($this->record->form->fields as $field) {

            $data = $field['data'] ;
            if ($field['type'] == 'form_field') {
                $className = 'Filament\Forms\Components\\'.$data['field_type'] ;
                //$InputClass = new $className() ;
                $component = $className::make($data['name'])
                    ->label($data['label']) ;
                if($data['required']) {
                    $component = $component->required() ;
                }
                if($data['description'] != null) {
                    $component = $component->helperText($data['description']) ;
                }
                if($data['field_type'] == 'TextInput') {
                    $component = $component->maxLength(255) ;
                }
                if($data['field_type'] != 'FileUpload') {
                    $component = $component->live(onBlur: true) ;
                } else {
                    $component = $component
                    ->live()
                    ->afterStateUpdated(function (Forms\Contracts\HasForms $livewire, Forms\Components\FileUpload $component, $state) { 
                        $livewire->validateOnly($component->getStatePath());
                        $filename = $state->store('form-attachments', 'private');
                        $answers = array_merge($this->record->answers, [$component->getName() => $filename]);
                        $this->record->update(['answers' => $answers]) ;
                    })
                    ->disk('private')
                    ->openable()
                    ->maxSize(1024)
                    ->acceptedFileTypes(['application/pdf', 'image/jpeg', 'image/png', 'image/webp'])
                    ->directory('form-attachments')
                    ->previewable(false) ;
                }
                
                if(isset($data['options']) && (sizeof($data['options']) > 0)) {
                    $component = $component->options($data['options']) ;
                }
            }

                        
            if ($field['type'] == 'heading') {
                $this->curent_data = $data ;
                $component = Placeholder::make('heading')
                ->content( new HtmlString('<'.$this->curent_data['level'].'>'.$this->curent_data['content'].'</'.$this->curent_data['content'].'>') ) 
                ->hiddenLabel() ;
            }
            if ($field['type'] == 'paragraph') {
                $this->curent_data = $data ;
                $component = Placeholder::make('paragraph')
                ->content( new HtmlString('<p>'.$this->curent_data['content'].'</p>') )
                ->hiddenLabel() ;
            }

        
            array_push($array_fileds, $component) ;

        }
        return $form
            ->schema([
                        
            Section::make('Fomulaire de réponse')
            ->aside() // This is the magic line!
            ->description('Contenu du formulaire rempli par le contact')
            ->schema($array_fileds)
                
            ])
            ->statePath('requestData')
            ->model($this->record);
    }

    protected function fillForms(): void
    {
        $data = $this->record->attributesToArray();
        
        $this->record->getAssignblesSteps() ;
        //Log::info(print_r($this->record, true));
        $this->stepForm->fill($data);
        $this->adminForm->fill($data);
        $this->requestForm->fill($data['answers']);
    }

    protected function getUpdateAdminFormActions(): array
    {
        return [
            Action::make('updateAdminAction')
                ->label(__('filament-panels::pages/auth/edit-profile.form.actions.save.label'))
                ->submit('AdminForm'),
        ];
    }

    public function getUpdateRequestFormActions(): array
    {
        return [
            Action::make('updatRequestAction')
                ->label(__('filament-panels::pages/auth/edit-profile.form.actions.save.label'))
                ->submit('RequestForm'),
        ];
    }

    public function updated($name, $value) 
    {
        //$this->updateRequest() ;
    }

    public function updateStep(): void
    {
        $data = $this->stepForm->getState();
        $this->handleRecordUpdate($this->record, $data);
        $this->record->step->transitionTo($data['step'], $data['instructions']);
        $this->sendSuccessNotification(); 
    }

    public function updateAdmin(): void
    {
        $data = $this->adminForm->getState();
        $this->handleRecordUpdate($this->record, $data);
        $this->sendSuccessNotification(); 
    }

    public function updateRequest(): void
    {
        $data = $this->requestForm->getState();
        
        /*Log::info(print_r('$this->record -----------------------', true));
        Log::info(print_r($data, true));
        
        Log::info(print_r('--------------------------------------', true));*/
        $answers = array_merge($this->record->answers, $data);
        $this->record->update(['answers' => $answers]) ;
        $this->sendSuccessNotification(); 
    }

    private function handleRecordUpdate(Model $record, array $data): Model
    {
        $record->update($data);
        return $record;
    }

    private function sendSuccessNotification(): void
    {
        Notification::make()
                ->success()
                ->title('Saved')
                ->send();
    }


}
