<?php

namespace App\Filament\Resources\EntryRequestResource\Pages;

use App\Filament\Resources\EntryRequestResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListEntryRequests extends ListRecords
{
    protected static string $resource = EntryRequestResource::class;

    
    protected static ?string $title = 'Soumissions';

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
