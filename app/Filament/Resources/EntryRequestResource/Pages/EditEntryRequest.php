<?php

namespace App\Filament\Resources\EntryRequestResource\Pages;

use App\Filament\Resources\EntryRequestResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Support\Facades\Log;
use App\Models\EntryRequest;

class EditEntryRequest extends EditRecord
{
    protected static string $resource = EntryRequestResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    protected function mutateFormDataBeforeSave(array $data): array
    {
        
        //Log::info(print_r('bbbbbb', true));
        $instructions = $data['instructions']? $data['instructions'] : '' ;
        $this->record->update($data) ;

        if($data['step'] != $this->record->step->getValue()) {
            $this->record->step->transitionTo($data['step'], $instructions);
        }
    
        return $data;
    }
}
