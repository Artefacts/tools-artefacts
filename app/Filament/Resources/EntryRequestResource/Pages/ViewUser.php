<?php

namespace App\Filament\Resources\EntryRequestResource\Pages;

use App\Filament\Resources\EntryRequestResource;
use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;

class ViewUser extends ViewRecord
{
    protected static string $resource = EntryRequestResource::class;
}
