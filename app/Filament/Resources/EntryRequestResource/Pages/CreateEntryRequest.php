<?php

namespace App\Filament\Resources\EntryRequestResource\Pages;

use App\Filament\Resources\EntryRequestResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateEntryRequest extends CreateRecord
{
    protected static string $resource = EntryRequestResource::class;
}
