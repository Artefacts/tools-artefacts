<?php

namespace App\Filament\Resources;

use App\Filament\Resources\EntryRequestResource\Pages;
use App\Filament\Resources\EntryRequestResource\RelationManagers;
use App\Models\EntryRequest;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Actions\Action;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\ToggleButtons;
use App\States\EntryRequest\Open;
use App\States\EntryRequest\Pulled;
use App\States\EntryRequest\Review;
use Illuminate\Support\Facades\Log;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Split;
use Filament\Forms\Get;
use Filament\Forms\Set;
use App\Livewire\EntryRequests\PublicEdit;
use Filament\Forms\Components\Livewire;
use Filament\Forms\Components\Grid;

class EntryRequestResource extends Resource
{
    protected static ?string $model = EntryRequest::class;

    protected static ?string $navigationIcon = 'heroicon-o-square-3-stack-3d';

    protected static ?string $navigationLabel = 'Soumissions';
    protected static ?string $title = 'Soumissions';
    protected static ?string $breadcrumb = 'Soumissions';

    public static function getNavigationBadge(): ?string
    {
        return static::getModel()::count();
    }
    public static function getNavigationBadgeColor(): ?string
    {
        return static::getModel()::count() > 10 ? 'warning' : 'info';
    }

    public static function form(Form $form): Form
    {
         
        return $form
            ->schema([
                Split::make([
                     
                    Grid::make([
                        'default' => 1,
                        'sm' => 2,
                        'md' => 3,
                        'lg' => 4,
                        'xl' => 6,
                        '2xl' => 8,
                    ])
                    ->schema([
                        Section::make('admin')
                        ->description('Settings for publishing this post.')
                        ->schema([
                            // ...
                                Forms\Components\Select::make('form_id')
                                ->relationship('form', 'name')
                                ->required(),
                                Forms\Components\TextInput::make('email')->regex('/^.+@.+$/i')
                                    ->required()
                                    ->maxLength(255),
                                Forms\Components\TextInput::make('admin_email')->regex('/^.+@.+$/i')
                                    ->maxLength(255),
                        ]),
                        /*Section::make('Answers')
                        ->description('Settings for publishing this post.')
                        ->schema([
                            // ...
                            Livewire::make(PublicEdit::class,
                            function (EntryRequest $EntryRequest) {
                                //Log::info(print_r($EntryRequest->step->getValue(), true));
                                return  ['id' => $EntryRequest->id];
                            } )
                            ->hidden(fn (?EntryRequest $EntryRequest): bool => $EntryRequest->form === null),
                        ]),*/
                    ]),
                    Section::make([
                        ToggleButtons::make('step')
                            ->options([
                                'open' => 'Open',
                                'pulled' => 'Pulled',
                                'review' => 'Review',
                                'published' => 'Published'
                            ])
                            ->colors([
                                'open' => 'info',
                                'pulled' => 'primary',
                                'review' => 'warning',
                                'published' => 'success',
                            ])
                            ->inline()
                            ->default(function (EntryRequest $EntryRequest) {
                                //Log::info(print_r($EntryRequest->step->getValue(), true));
                                return $EntryRequest->step->getValue() ;
                            })
                            ->afterStateUpdated(function (Set $set, $state) {
                                /*$class = null ; 
                                if ($state == 'open') {
                                    $class = Open::class ;
                                }
                                if ($state == 'pulled') {
                                    $class = Pulled::class ;
                                }
                                if ($state == 'review') {
                                    $class = Review::class ;
                                }
                                $set('step', $class);*/
                            }),
                        Textarea::make('instructions')
                            ->rows(10),
                    ])->grow(false),
                ])->from('md')->columnSpan(2)

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->searchable(),
                Tables\Columns\TextColumn::make('email')
                    ->searchable(),
                Tables\Columns\TextColumn::make('step')
                    ->label('Etape')
                    ->badge()
                    ->color(fn (string $state): string => match ($state) {
                        'open' => 'gray',
                        'pulled' => 'warning',
                        'review' => 'warning',
                        'accepted' => 'success',
                        'closed' => 'danger',
                    })
                        ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(),
            ])
            ->filters([
                SelectFilter::make('step')
                    ->options([
                        'open' => 'Open',
                        'pulled' => 'Pulled',
                        'review' => 'Review',
                    ])
            ])
            ->actions([
                Action::make('check')
                ->label('Vérifier')
                ->url(fn (EntryRequest $record): string => route('filament.admin.resources.entry-requests.check', $record)),
                /*Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),
                Action::make('change step')
                    ->form([
                        ToggleButtons::make('step')
                            ->options([
                                'open' => 'Open',
                                'pulled' => 'Pulled',
                                'review' => 'Review',
                                'published' => 'Published'
                            ])
                            ->colors([
                                'open' => 'info',
                                'pulled' => 'primary',
                                'review' => 'warning',
                                'published' => 'success',
                            ])
                            ->inline()
                            ->default(function (EntryRequest $EntryRequest) {
                                Log::info(print_r($EntryRequest->step->getValue(), true));
                                return $EntryRequest->step->getValue() ;
                            }),
                        Textarea::make('instructions')
                            ->rows(10)
                            ->default(function (EntryRequest $EntryRequest) {
                                return $EntryRequest->instructions ;
                            }),
                    ])
                    ->action(function (EntryRequest $EntryRequest, array $data): void {
                        $class = null ; 
                        if ($data['step'] == 'open') {
                            $class = Open::class ;
                        }
                        if ($data['step'] == 'pulled') {
                            $class = Pulled::class ;
                        }
                        if ($data['step'] == 'review') {
                            $class = Review::class ;
                        }
                        if ((string)$EntryRequest->step != $data['step']) {
                            // Change step and save
                            $EntryRequest->step->transitionTo($class, $data['instructions']);
                        } else {
                            // Save if new instructions
                            $EntryRequest->save() ;
                        }
                    })*/
            ])
            ->recordUrl(fn (EntryRequest $record): string => route('filament.admin.resources.entry-requests.check', $record))
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListEntryRequests::route('/'),
            'create' => Pages\CreateEntryRequest::route('/create'),
            'edit' => Pages\EditEntryRequest::route('/{record}/edit'),
            'check' => Pages\CheckEntryRequest::route('/{record}/check'),
            'view' => Pages\ViewEntryRequest::route('/{record}'),
        ];
    }
}
