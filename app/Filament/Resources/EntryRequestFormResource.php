<?php

namespace App\Filament\Resources;

use App\Filament\Resources\EntryRequestFormResource\Pages;
use App\Filament\Resources\EntryRequestFormResource\RelationManagers;
use App\Models\EntryRequestForm;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
//use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\Builder;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Illuminate\Support\Str;
use Filament\Forms\Components\Section;

class EntryRequestFormResource extends Resource
{
    protected static ?string $model = EntryRequestForm::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Information générales')
                    ->aside() // This is the magic line!
                    ->description('Nom du formulaire, instruction générales, status')
                    ->schema([
                        Forms\Components\TextInput::make('name')
                            ->helperText('Nom utilisé pour l\'administration.')
                            ->required()
                            ->maxLength(255),
                        Forms\Components\Textarea::make('description')
                            ->helperText('Description utilisée pour l\'administration.')
                            ->required()
                            ->maxLength(255),
                        Forms\Components\Toggle::make('open')
                            ->helperText('Ouvrir ou bloquer l\'acces à ce formulaire. Si bloqué, il ne peut plus être remplit par les utilisateurs.')
                            ->required(),
                
                    ]),
                Section::make('Composition du formulaire')
                    ->aside() // This is the magic line!
                    ->description('Liste de champs et informations annexes à insérer pour coposer le formulaire à remplir pour les spersonnes.')
                    ->schema([
                        Builder::make('fields')
                            ->columnSpan([
                                'sm' => 2,
                                'xl' => 3,
                                '2xl' => 4,
                            ])
                            ->blocks([
                                Builder\Block::make('heading')
                                    ->schema([
                                        TextInput::make('content')
                                            ->label('Heading')
                                            ->required(),
                                        Select::make('level')
                                            ->options([
                                                'h1' => 'Heading 1',
                                                'h2' => 'Heading 2',
                                                'h3' => 'Heading 3',
                                                'h4' => 'Heading 4',
                                                'h5' => 'Heading 5',
                                                'h6' => 'Heading 6',
                                            ])
                                            ->required(),
                                    ])
                                    ->columns(2),
                                Builder\Block::make('paragraph')
                                    ->schema([
                                        Textarea::make('content')
                                            ->label('Paragraph')
                                            ->required(),
                                    ]),
                                Builder\Block::make('image')
                                    ->schema([
                                        FileUpload::make('url')
                                            ->label('Image')
                                            ->image()
                                            ->required(),
                                        TextInput::make('alt')
                                            ->label('Alt text')
                                            ->required(),
                                    ]),
                                
                                Builder\Block::make('form_field')
                                    ->label('Champs de formulaire')
                                    ->schema([
                                        TextInput::make('label')
                                            ->label('Label')
                                            ->helperText('Titre associé au champs.')
                                            ->live(onBlur: true)
                                            ->afterStateUpdated(fn (Set $set, ?string $state) => $set('name', Str::slug($state)))
                                            ->required(),
                                        TextInput::make('name')
                                            ->label('Nom du champs')
                                            ->helperText('Nom unique donné au champs pour le formulaire - utilise le label du champs pour générer un nom (caractères autorisés sans espaces [a-z], [0-9] et les tirets [-_]')
                                            ->required(),  
                                        
                                        Select::make('field_type')
                                            ->label('Type de champ')
                                            ->helperText('Choisissez le widget à utiliser pour collecter l\'information demandée.')
                                            ->live()
                                            ->options([
                                                'TextInput' => 'Texte',
                                                'Textarea' => 'Texte long',
                                                'DatePicker' => 'Date',
                                                'FileUpload' => 'Fichier',
                                                'Toggle' => 'Toggle',
                                                'Select' => 'Liste déroulante',
                                                'Radio' => 'Cases à cocher (choix unique)',
                                                'CheckboxList' => 'Cases à cocher',
                                            ])
                                            ->required(),  
                                        Textarea::make('description')
                                            ->helperText('Sera affiché comme ce texte sous le champs.')
                                            ->label('Texte d\'aide'),
                                        Forms\Components\Toggle::make('required')
                                            ->label('Obligatoire')
                                            ->helperText('Activé si le champs est obligatoire.')
                                            ->default(true),
                                        Forms\Components\KeyValue::make('liste')::make('options')
                                            ->reorderable()
                                            ->keyLabel('Clé (alphanumerique)')
                                            ->valueLabel('Titre du choix')
                                            ->visible(fn (Get $get): bool => in_array($get('field_type'), ['Select','Radio','CheckboxList' ]))
                                            ->required(fn (Get $get): bool => in_array($get('field_type'), ['Select','Radio','CheckboxList' ]))->columnSpan(2),
                                    ])
                                    ->columns(2),
                            ])->collapsed()->cloneable()
                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->searchable(),
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\IconColumn::make('open')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListEntryRequestForms::route('/'),
            'create' => Pages\CreateEntryRequestForm::route('/create'),
            'edit' => Pages\EditEntryRequestForm::route('/{record}/edit'),
        ];
    }
}
