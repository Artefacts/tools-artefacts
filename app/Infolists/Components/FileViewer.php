<?php

namespace App\Infolists\Components;

use Filament\Infolists\Components\Entry;
use Illuminate\Support\Facades\Storage;

class FileViewer extends Entry
{
    protected string $view = 'infolists.components.file-viewer';

    public function getMime($value) {
        return Storage::mimeType('private/'.$value);
    }
}
