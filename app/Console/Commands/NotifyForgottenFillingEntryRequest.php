<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EntryRequest;
use Illuminate\Support\Carbon;

class NotifyForgottenFillingEntryRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:notify-forgotten-filling-entry-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $EntryRequests = EntryRequest::query()->where('updated_at', '<', now()->subMinutes(10));
        $count = $EntryRequests->count() ;
        $this->info("Sending email forgotten to {$count} requests!");
        foreach($EntryRequests->get() as $EntryRequest) {
            event('eloquent.forgottenFilling: ' . EntryRequest::class, $EntryRequest);
        }
    }
}

