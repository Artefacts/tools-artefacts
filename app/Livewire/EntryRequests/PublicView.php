<?php

namespace App\Livewire\EntryRequests;

use App\Models\EntryRequest;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Infolists\Concerns\InteractsWithInfolists;
use Filament\Infolists\Contracts\HasInfolists;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Infolist;
use Livewire\Component;

class PublicView extends Component implements HasForms, HasInfolists
{
    
    use InteractsWithInfolists;
    use InteractsWithForms;

    public EntryRequest $record;

    public function mount($id): void
    {
        $this->record = EntryRequest::findOrFail($id);
    }

    public function render()
    {
        return view('livewire.entry-requests.public-view');
    }
    public function requestInfolist(Infolist $infolist): Infolist
    {
        return $infolist
            ->record($this->record)
            ->schema([
                TextEntry::make('info_1'),
                TextEntry::make('fichier_1'),
                TextEntry::make('info_2'),
                TextEntry::make('fichier_2'),
                // ...
            ]);
    }
}
