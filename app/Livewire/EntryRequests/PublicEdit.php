<?php

namespace App\Livewire\EntryRequests;

use App\Models\EntryRequest;
use Filament\Forms;
use Illuminate\Validation\Rule;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Livewire\Component;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;
use App\States\EntryRequest\Pulled;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Wizard;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\HtmlString;

class PublicEdit extends Component implements HasForms
{
    use InteractsWithForms;

    public ?array $data = [];

    public EntryRequest $record;

    public function mount($id): void
    {
        $this->record = EntryRequest::findOrFail($id);
        if($this->record->answers == null) {
            $this->record->answers = [] ;
        }
        $this->form->fill($this->record->answers);
    }

    public function form(Form $form): Form
    {
        if($this->record->answers == null) {
            $this->record->answers = [] ;
        }
        /* field type data dump
            [data] => Array
                (
                    [name] => sdfdfd
                    [label] => sdfdfd
                    [options] => Array ()
                    [required] => 1
                    [field_type] => TextInput
                    [description] => 
                )
            [type] => form_field
        */
        
        //Log::info(print_r($this->record->form->fields, true));
        $array_fileds = [] ; 
        $values_fields = $this->record->answers ;
        foreach ($this->record->form->fields as $field) {

            $data = $field['data'] ;
            if ($field['type'] == 'form_field') {
                $className = 'Filament\Forms\Components\\'.$data['field_type'] ;
                //$InputClass = new $className() ;
                $component = $className::make($data['name'])
                    ->label($data['label']) ;
                if($data['required']) {
                    $component = $component->required() ;
                }
                if($data['description'] != null) {
                    $component = $component->helperText($data['description']) ;
                }
                if($data['field_type'] == 'TextInput') {
                    $component = $component->maxLength(255) ;
                }
                if($data['field_type'] != 'FileUpload') {
                    $component = $component->live(onBlur: true) ;
                } else {
                    $component = $component
                    ->live()
                    ->afterStateUpdated(function (Forms\Contracts\HasForms $livewire, Forms\Components\FileUpload $component, $state) { 
                        $livewire->validateOnly($component->getStatePath());
                        $filename = $state->store('form-attachments', 'private');
                        $answers = array_merge($this->record->answers, [$component->getName() => $filename]);
                        $this->record->update(['answers' => $answers]) ;
                    })
                    ->disk('private')
                    ->openable()
                    ->maxSize(1024)
                    ->acceptedFileTypes(['application/pdf', 'image/jpeg', 'image/png', 'image/webp'])
                    ->directory('form-attachments')
                    ->previewable(false) ;
                }
                
                if(isset($data['options']) && (sizeof($data['options']) > 0)) {
                    $component = $component->options($data['options']) ;
                }
            }

            /* Contents data dump 
             [0] => Array
                (
                    [data] => Array
                        (
                            [level] => h2
                            [content] => Merci de ta participation
                        )
                    [type] => heading
                )
            [1] => Array
                (
                    [data] => Array
                        (
                            [content] => Un texte de description
                        )
                    [type] => paragraph
                )  */
            
            if ($field['type'] == 'heading') {
                $this->curent_data = $data ;
                $component = Placeholder::make('heading')
                ->content( new HtmlString('<'.$this->curent_data['level'].'>'.$this->curent_data['content'].'</'.$this->curent_data['content'].'>') ) 
                ->hiddenLabel() ;
            }
            if ($field['type'] == 'paragraph') {
                $this->curent_data = $data ;
                $component = Placeholder::make('paragraph')
                ->content( new HtmlString('<p>'.$this->curent_data['content'].'</p>') )
                ->hiddenLabel() ;
            }
            
            array_push($array_fileds, $component) ;

        }
        return $form
            ->schema($array_fileds)
            ->statePath('data')
            ->model($this->record);
    }

    
    public function updated($name, $value) 
    {
        $data = $this->validateOnly($name);
        //Log::info(print_r($this->form->getState(), true));
        if (isset($data['data'])) {
            $answers = array_merge($this->record->answers, $data['data']);
            $this->record->update(['answers' => $answers]) ;
        }
    }

    public function save()
    {
        $data = $this->form->getState();

        $answers = array_merge($this->record->answers, $data);
        $this->record->update(['answers' => $answers]) ;
        
        $this->record->step->transitionTo(Pulled::class, '');
        //$this->record->save() ;

        return $this->redirect('/requests/view/'.$this->record->id);
    }

    public function render(): View
    {
        return view('livewire.entry-requests.public-edit');
    }
}
