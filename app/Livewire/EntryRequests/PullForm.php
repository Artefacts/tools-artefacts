<?php

namespace App\Livewire\EntryRequests;

use App\Models\EntryRequest;
use Filament\Forms;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Livewire\Component;
use Illuminate\Contracts\View\View;
use Filament\Forms\Components\Hidden;
use App\States\EntryRequest\Pulled;


class PullForm extends Component implements HasForms
{
    use InteractsWithForms;

    public ?array $data = [];

    public EntryRequest $record;

    public function mount($id): void
    {
        $this->record = EntryRequest::findOrFail($id);
        $this->form->fill($this->record->attributesToArray());
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                Hidden::make('step')
                    ->default('pulled')
                    ->disabled()
            ])
            ->statePath('data')
            ->model($this->record);
    }

    public function save(): void
    {
        $data = $this->form->getState();
        $this->record->step->transitionTo(Pulled::class, '');
        $this->record->update($data);
    }

    public function render(): View
    {
        return view('livewire.entry-requests.pull-form');
    }
}
