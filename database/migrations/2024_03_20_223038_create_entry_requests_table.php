<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entry_requests', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('info_1')->nullable($value = true);
            $table->string('fichier_1')->nullable($value = true);
            $table->string('info_2')->nullable($value = true);
            $table->string('fichier_2')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entry_requests');
    }
};
