<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entry_request_logs', function (Blueprint $table) {
            $table->id();
            $table->json('ref')->nullable($value = true);
            $table->json('diff')->nullable($value = true);
            $table->foreignIdFor(\App\Models\EntryRequest::class)
            ->nullable()
            ->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entry_request_logs');
    }
};
