<?php

use App\Livewire\EntryRequests\PublicEdit;
use App\Livewire\EntryRequests\PublicView;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;


use App\Http\Controllers\FileAccessController ;

Route::get('/private/download/form-attachments/{file}', [FileAccessController::class, 'download']);

Route::get('/private/view/form-attachments/{file}', [FileAccessController::class, 'serve']);

Route::get('/', function () {
    return view('welcome');
});

Route::get('requests/edit/{id}', PublicEdit::class)->setDefaults([ 'model' => App\Models\EntryRequest::class]);
Route::get('requests/view/{id}', PublicView::class)->setDefaults([ 'model' => App\Models\EntryRequest::class]);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
